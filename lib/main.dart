import 'package:flutter/material.dart';
import 'file:///C:\Users\Herna\Desktop\login_with_shared_preferences-master\lib\views\login.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var assetsImage = new AssetImage('assets/images/logo.jpg');
    var image = new Image(
      image: assetsImage,
      width: 120,
    );

    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Login"),
          backgroundColor: Colors.amber[600],
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 32),
          child: LoginView(),
        ),
      ),
    );
  }
}
