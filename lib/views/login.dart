import 'package:flutter/material.dart';
import 'package:tarea_flutter_app/utils/data.dart';
import 'package:tarea_flutter_app/views/logout.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  var image =
      new Image(image: new AssetImage('assets/images/logo.jpg'), width: 240);

  TextEditingController _usuario = TextEditingController();
  TextEditingController _password = TextEditingController();

  Widget createUserInput() {
    return Padding(
      padding: EdgeInsets.only(top: 40),
      child: TextField(
        controller: _usuario,
        textCapitalization: TextCapitalization.sentences,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Ingrese usuario',
          labelText: 'Usuario',
          helperText: 'Solo se acepta usuarios. Ejemplo: User',
          suffixIcon: Icon(Icons.people),
          icon: Icon(Icons.account_circle),
        ),
        onChanged: (value) => _guardarDatos(),
      ),
    );
  }

  Widget createPasswordInput() {
    return Padding(
      padding: EdgeInsets.only(top: 20),
      child: TextField(
        controller: _password,
        decoration: InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Inserte su contraseña',
          labelText: 'Contraseña',
          helperText: 'Ingrese su contraseña',
          suffixIcon: Icon(Icons.remove_red_eye),
          icon: Icon(Icons.lock),
        ),
        obscureText: true,
        onChanged: (value) => _guardarDatos(),
      ),
    );
  }

  Widget createLoginButton() {
    return Padding(
      padding: EdgeInsets.only(top: 40),
      child: RaisedButton(
        child: Text('Ingresar'),
        onPressed: () {
          Route route = MaterialPageRoute(builder: (bc) => LogoutView());
          Navigator.of(context).push(route);
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _obtenerDatos(datos: ['usuario', 'password']);
  }

  @override
  Widget build(BuildContext context) {
    return ListView(children: [
      Padding(
          padding: EdgeInsets.only(top: 20),
          child: Center(child: Container(child: image))),
      createUserInput(),
      createPasswordInput(),
      createLoginButton(),
    ]);
  }

  void _obtenerDatos({List<String> datos}) async {
    for (String usuario in datos) {
      bool exist = await Data().checkData(usuario);
      if (exist) {
        String datoObtenido = await Data().getData(usuario);
        if (usuario == 'usuario') {
          _usuario.text = datoObtenido;
        } else {
          _password.text = datoObtenido;
        }
      }
    }
    setState(() {});
  }

  void _guardarDatos() async {
    await Data().saveData('usuario', _usuario.text);
    await Data().saveData('password', _password.text);

    setState(() {});
  }

  void _eliminarDatos() async {
    _usuario.text = '';
    _password.text = '';
    await Data().deleteData('usuario');
    await Data().deleteData('password');

    setState(() {});
  }
}
