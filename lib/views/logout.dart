import 'package:flutter/material.dart';
import 'package:tarea_flutter_app/utils/data.dart';
import 'package:tarea_flutter_app/views/login.dart';

class LogoutView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Logout'),
      ),
      body: Center(
          child: RaisedButton(
        child: Text('Salir'),
        onPressed: () {
          Navigator.of(context).pop();
        },
      )),
    );
  }
}
