# Trabajo - Taller de tecnologias moviles

**Nombre:** Hernan Cid
**Carrera:** Ingenieria Informatica

## Estado entrega
 
El trabajo consiste en lo siguiente:

1.- Crear una aplicación con múltiples páginas (al menos dos) y rutas.
2.- La primera página será un Login (usuario, contraseña), el login
comprobará usuarios desde un json o Map local.
3.- Si el usuario se ha loggeado, la aplicación deberá recordar esta
acción, por lo que si accedo una segunda vez me lanzará a la segunda
pantalla (home) y no me mostrará el login.
4.- Por último, agregar un botón de logout (cerrar sesión) el cual borre
mis datos y me envíe al login.

De los puntos anteriores solo se pudieron cumplir los siguientes puntos:

1. Se crearon dos paginas sencillas para probar el funcionamiento solicitado, un loggin
   y una vista con un boton de logout para probar el punto 4.
2. Se creo la pagina login con sus respectivos elementos y el boton Ingresar es "funcional",
   falto comprobar el usuario en  el archivo json.
3. Los datos ingresados quedan almacenados mediante shared_preferences, por lo que es funcional
   este apartado.